add_clang_executable(clang-for-offload ForList.cpp ForOffload.cpp)

target_link_libraries(clang-for-offload clangTooling)

install(TARGETS clang-for-offload
        RUNTIME DESTINATION bin)
